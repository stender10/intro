import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Answer {

   public static void main (String[] param) {

      // TODO!!! Solutions to small problems 
      //   that do not need an independent method!

      // conversion double -> String
      String s1 = String.valueOf(Math.PI);
      System.out.println(s1);

      // conversion String -> int
      int i1 = Integer.parseInt("10");
      System.out.println(i1);

      // "hh:mm:ss"
      DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
      Date date = new Date();
      System.out.println("Current time is: " + dateFormat.format(date));

      // cos 45 deg
      System.out.println("cos(45) = " + Math.cos(45));

      // table of square roots
      int lastNumber = 10;
      System.out.println("table of square roots to " + lastNumber);
      System.out.println("-----------------");
      for (int i = 1; i < lastNumber + 1; i++) {
         System.out.println(i + " " + (int) Math.pow(i, 2));
      }
      System.out.println("-----------------");
      String firstString = "ABcd12";
      String result = reverseCase (firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse string

      String s = "How  many	 words   here";
      int nw = countWords (s);
      System.out.println (s + "\t" + String.valueOf (nw));

      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!
      long t0 = System.nanoTime();
      try {
         Thread.sleep(3000);
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
      long t1 = System.nanoTime();
      System.out.println("vahe oli: " + (t1 - t0)/1000000);

      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<Integer> (LSIZE);
      Random generaator = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (Integer.valueOf (generaator.nextInt(1000)));
      }

      // minimal element

      // HashMap tasks:
      //    create
      //    print all keys
      //    remove a key
      //    print all pairs

      System.out.println ("Before reverse:  " + randList);
      reverseList (randList);
      System.out.println ("After reverse: " + randList);

      System.out.println ("Maximum: " + maximum (randList));
   }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
         T maximum (Collection<? extends T> a)
            throws NoSuchElementException {
      return Collections.max(a);
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {
      return new StringTokenizer(text).countTokens();
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    */
   public static String reverseCase (String s) {
      char[] charArray = s.toCharArray();

      for (int i = 0; i < charArray.length; i++) {
         if (Character.isUpperCase(charArray[i])) {
            charArray[i] = Character.toLowerCase(charArray[i]);
         } else if (Character.isLowerCase(charArray[i])) {
            charArray[i] = Character.toUpperCase(charArray[i]);
         }
      }

      System.out.println("initial: " + s + "\n" +
              "reverse case: " + String.valueOf(charArray));

      return String.valueOf(charArray); // TODO!!! Your code here
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T extends Object> void reverseList (List<T> list)
      throws UnsupportedOperationException {
      Collections.reverse(list);
   }
}
